def sum_primes(n):
    sum, sieve = 2, [True] * n
    for p in range(3, n, 2):
        if sieve[p]:
            sum += p
            for i in range(p*p, n, p):
                sieve[i] = False
    return sum

print(sum_primes(2000000))